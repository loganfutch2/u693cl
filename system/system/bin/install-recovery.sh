#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:67108864:3ae598472d99f015a3bc7f6f556cd71ee91a80d2; then
  applypatch  EMMC:/dev/block/bootdevice/by-name/boot:67108864:f189761751c6488c07fa3cf01c00607fc0355f5e EMMC:/dev/block/bootdevice/by-name/recovery 3ae598472d99f015a3bc7f6f556cd71ee91a80d2 67108864 f189761751c6488c07fa3cf01c00607fc0355f5e:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
